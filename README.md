# UCD Instana Plug-In

## set gradle and java version

~~~sh
 8308  curl -s "https://get.sdkman.io" | zsh
 8311  sdk list gradle
 8312  sdk install gradle 4.0
 8313  /usr/libexec/java_home -V
 8314  export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_345`
 8316  java -version
 8317  gradle
~~~

## Need to check

* which version of GRADLE and GROOVY is supported
* step for plugin
* naming of plugin